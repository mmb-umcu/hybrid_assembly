__**Hybrid Assembly pipeline on the HPC**__
================

# Table of Contents
* [Intro: What are we trying to achieve?](#intro-what-are-we-trying-to-achieve)
* [Usage 1: Singularity container](#usage-1-singularity-container)
  * [1.1. Requirements](#11-requirements)
  * [1.2. Usage: regular run](#12-usage-regular-run)
  * [1.3. Usage: assembly completion run](#13-usage-assembly-completion-run)
  * [1.4. Usage: Pro singularity user](#14-usage-pro-singularity-user)
  * [1.5. Help](#14-help)
* [Usage 2: Git repository clone](#usage-2-git-repository-clone)
  * [2.1. Requirements](#21-requirements)
  * [2.2. Usage: regular run](#22-usage-regular-run)
  * [2.3. Usage: assembly completion run](#23-usage-assembly-completion-run)
  * [2.4. Help](#24-help)
* [Rotating circular sequences](#rotating-circular-sequences)
* [Troubleshooting](#troubleshooting)
  * [Singularity container - Out of memory (OOM) error](#singularity-container-out-of-memory-oom-error)
  * [Pilon - Java memory error](#pilon-java-memory-error)

# Intro: What are we trying to achieve?

In order to perform hybrid assemblies, we came up with a Snakemake pipeline that integrates different steps:

  - **Trimming** Illumina reads with *trim_galore* 
  - Perform a **first short-read assembly** using *Unicycler*. It will only run *SPAdes* but will generate a *.fasta file from which we can estimate the genome size of the isolate. 
  - Calculate the **genome size** of the isolate using *bioawk*. 
  - **Filter ONT reads** using *filtlong* indicating the quality filtering the Illumina reads and indicating the **coverage** specified by the user. This is why the calculation of the genome size it is important. 
  - Perform a **hybrid assembly** using *Unicycler* using the trimmed Illumina reads and filtered ONT reads.

----

# Usage 1: Singularity container

With this usage method for the hybrid assembly pipeline, **you do not have to install or setup anything yourself**. The tool is ready for use, by accessing a software container we already have installed in the HPC, that you can run from your analysis directory.

An important note here is that you will need the data to be present within the directory you are working on (your working directory), or in a subdirectory of your working directory. For example, if you are working in directory `my_hybrid_assembly` your data can be inside a subdirectory `my_hybrid_assembly/data`. Symlinks that direct to directories **outside** of the file tree within `my_hybrid_assembly` **will not work** with the Singularity container.

### 1.1. Requirements

  - Singularity installed in your system. This tutorial is designed for the MMB-UMCU group and assumes that you are working in the HPC, where Singularity is installed. Therefore, you should have an HPC account and knowledge of how to use the HPC.
  - A subdirectory within your working directory, containing your data to be analysed (short and long reads).

### 1.2. Usage: regular run

Thus, to run the pipeline, you can use a command like:
``` bash
sbatch hybrid-assembly-v1.0 \
    -f data/short_reads/[forward_short_reads].fastq.gz \
    -r data/short_reads/[reverse_short_reads].fastq.gz \
    -l data/long_reads/[long_reads].fastq.gz \
    -n [sample_name] -M [mode] -c [coverage] -p [Phred score]
```

Or, in one line:
``` bash
sbatch hybrid-assembly-v1.0 -f data/short_reads/[forward_short_reads].fastq.gz -r data/short_reads/[reverse_short_reads].fastq.gz -l data/long_reads/[long_reads].fastq.gz -n [sample_name] -M [mode] -c [coverage] -p [Phred score]
```

**If you are comfortable with the HPC**, you may, in some scenarios (e.g. troubleshooting), want to work in an [[interactive_session_on_the_hpc|interactive session]] with `srun`, instead of sending an `sbatch` job. To do this, first start your `srun` session -- allocate approximately 12 to 24 hours of runtime, 10 GB of memory and 8 CPUs -- , and then start the pipeline normally, but start the command with `bash` (instead of `sbatch`).

### 1.3. Usage: assembly completion run

To run the pipeline in assembly completion mode, you just have to add the argument `-C`:
``` bash
sbatch hybrid-assembly-v1.0 -C \
    -f data/short_reads/[forward_short_reads].fastq.gz \
    -r data/short_reads/[reverse_short_reads].fastq.gz \
    -l data/long_reads/[long_reads].fastq.gz \
    -n [sample_name] -M [mode] -c [coverage] -p [Phred score]
```

Here, we advise to use a lower coverage -- a rule of thumb value is 5: `-c 5`.

### 1.4. Usage: Pro singularity user

If you want to avoid using the `hybrid-assembly-v1.0` script and use the singularity container yourself, you can...

First pull the container:
``` bash
singularity pull docker://mmbumcu/hybrid_assembly:v1.0
```

Then copy some necessary files from the container to your workdir:
``` bash
singularity exec -H $PWD hybrid_assembly_v1.0.sif cp -r /opt/hybrid_assembly/{config,db,src} $PWD
```

Then run your analysis (adapt arguments as needed):
``` bash
singularity exec -H $PWD -B $TMPDIR,$PWD:/data --pwd data hybrid_assembly_v1.0.sif runassembly.sh \
    -S -f $forward -r $reverse -l $long_reads -n $name -M $mode -c $coverage -p $phred_score
```

### 1.5. Help

The name of the script you need to access, for the hybrid assembly pipeline **version v1.0**, is just `hybrid-assembly-v1.0`.

You can check the Help message of this script to find out what arguments it expects, with
``` bash
bash hybrid-assembly-v1.0 -h
```

This will print the following message:
```
Hybrid Assembly Pipeline version v1.0
=====================================

sbatch hybrid-assembly-v1.0 [-S <file>] [-f <file>] [-r <file>] [-l <file>] [ -L <file>] [-c <int>] [...]

Input:
-S <file>   [Optional] Path to a Singularity Image File (SIF) to execute the pipeline.
            If none is provided, will try to find file 'hybrid-assembly-v1.0' in $PATH.
-f <file>   [MANDATORY] Short forward strand reads in FASTQ format (.fastq or .fastq.gz)
-r <file>   [MANDATORY] Short reverse strand reads in FASTQ format (.fastq or .fastq.gz)
-l <file>   [MANDATORY] Long reads in FASTQ format (.fastq or .fastq.gz)
-L <file>   [Optional] Existing long read assembly in GFA format

Output:
-n <str>    [Optional] Sample name, ID or alias

Parameters:
-C          [Optioanl] Run assembly completion mode
-M <str>    [Optional] Choose mode from list: conservative, normal, bold
-c <int>    [MANDATORY] Minimum coverage to subset long reads -- for a typical analysis,
            we recommend a value of '20'; for completion mode, we recommend a value of '5'
-p <int>    [default=20] Minimum Phred score threshold for short read trimming
  
Others:
-v          Print pipeline version.
-h          Print this Help page and exit.
```

----

# Usage 2: Git repository clone

### 2.1. Requirements

First, you need to retrieve all the files required (snakemake files, yaml files for conda...). You can clone the following repo:

``` bash
git clone https://gitlab.com/mmb-umcu/hybrid_assembly.git
```

__Ok, now let's go to the hpc!__ 

Do you have your own miniconda installation? The following command should point to your user. 

``` bash
which conda
```

Then, create a separate environment with snakemake. 
``` bash
conda activate my_env_with_snakemake
snakemake
```

This environment is important since it will be the first one used in the pipeline to throw Snakemake. If there is no snakemake, it won't run the assembly. 

### 2.2. Usage: regular run

So if you want to run the pipeline, we created a bash wrapper that you should use in the following way:

``` bash
sbatch runassembly.sh -f path_forward_Illumina.fastq.gz -r path_reverse_Illumina.fastq.gz -l path_long_reads.fastq.gz -c 20 -p 20 -n name_isolate
```

The assembly file (*.gfa or *.fasta) with the hybrid assembly will be present in the folder:

  * long_read_assembly/[name_isolate]_unicycler

You can check the completion of the assembly by: 

``` bash
grep '>' long_read_assembly/[name_isolate]_unicycler/assembly.fasta
```

If all the contigs popping up have a flag saying `circular=True`, it means that you have a perfect assembly, great take a coffee. 


### 2.3. Usage: assembly completion run

If you get uncircularised contigs in the final assembly, you can run the second bash script: 

SLURM:
``` bash
sbatch completeassembly.sh -f path_forward_Illumina.fastq.gz -r path_reverse_Illumina.fastq.gz -l path_long_reads.fastq.gz -c 5 -p 20 -n name_isolate_path_unicycler
```

This script will take the contigs 'uncircularised' from the graph, and take as many reads as indicated in the coverage (in the example 5). These reads will be combined with the reads that we used before. In this way we hope to put some extra reads in the uncompleted path that can help Unicycler to bridge it. The new results will be present as: 

  * long_read_assembly/name_isolate_path_unicycler/assembly.fasta 

If after this, you still get uncircularised contigs. There are two scenarios possible: 

  * You have a linear plasmid. This can be the case if the contig is a completely independent component in the graph. 
  * You need to increase the coverage. If that is the case, run 'runassembly.sh' with a coverage (flag -c) higher than 20. This is the case if you have a component in the graph in which you have several contigs connected by a link. 

### 2.4. Help

----

# Rotating circular sequences

Within Unicycler, the determination of the starting point for completely circular sequences is determined by the replicon database provided. If you need to rotate assembled sequences after running Unicycler (we did it for the bacterial chromosome, to start at the DnaA gene), you can use Circlator.

You can install Circlator using conda:

``` bash
conda install -c bioconda circlator
```
 
Then you can use the function 'fixstart' to rotate the sequences.
 
``` bash
circlator fixstart --genes_fa replicon_database.fasta assembly.fasta output_file_prefix
```
 
----

# Troubleshooting

### Singularity container - Out of memory (OOM) error
If you start run the pipeline in Singularity container mode, with `sbatch` and with default settings, you may in some cases get an "out of memory" (OOM) error, especially if your minimum coverage `-c` setting is much higher than `-c 20`. In this case, you can change the memory request to the HPC, by adding the option `--mem`:

``` bash
sbatch --mem=[memory in GB] hybrid-assembly-v1.0 \
    -f data/short_reads/[forward_short_reads].fastq.gz \
    -r data/short_reads/[reverse_short_reads].fastq.gz \
    -l data/long_reads/[long_reads].fastq.gz \
    -n [sample_name] -M [mode] -c [coverage] -p [Phred score]
```

You can request in `--mem` however much memory you like/is allowed. For example, `--mem=60G` corresponds to a memory request of 60 GB.

Optionally, if you are an advanced user, you can also start the run inside an interactive `srun` session instead, with your desired resource requests.

### Pilon - Java memory error

There is a known issue with Pilon hardcoding the maximum amount of memory. If you encounter it, you can fix this issue by changing the code in your Pilon installation. First, locate where Pilon is installed:

``` bash
which pilon
```

This will give you an output somewhat like this -- **note that this is __my__ installation, do not copy it! you must find yours**:
``` bash
/hpc/dla_mm/rsilva/data/miniconda3/envs/unicycler/bin/pilon
```

Then, change this file with a text editor (e.g. nano or vim), and on line 16 you will see this piece of code:

```
default_jvm_mem_opts = ['-Xms512m', '-Xmx1g']
```

If you encountered this problem, then likely the second option you see in this line is `-Xmx1g`, which tells Java to allocate a maximum of 1G to the memory pool upon execution. Change this to a higher value you deem fit (within reason). For my own work, I found 8G to be more than enough:

```
default_jvm_mem_opts = ['-Xms512m', '-Xmx8g']
```
