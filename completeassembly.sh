#!/bin/bash

## SLURM queue arguments
#SBATCH -J completeassembly   ## job name
#SBATCH -t 24:00:00           ## time slot requested
#SBATCH --mem=2G              ## memory request
#SBATCH -o %x-%j.out          ## STDOUT log file
#SBATCH -o %x-%j.out          ## STDERR log file

## to debug
#set -e
#set -v
#set -x

# Change version here
version="v1.0"

# Print pipeline version
printversion() {
  cat <<VERSION

  Hybrid Assembly Pipeline version ${version}
  =====================================
  
VERSION
}

# Help message
usage() {
  cat <<HELP_USAGE
`printversion`
  sbatch $(basename $0) [-f <file>] [-r <file>] [-l <file>] [-c <int>] [-E <str>] [...]

  Input:
  -f <file>   [MANDATORY] Short forward strand reads in FASTQ format (.fastq or .fastq.gz)
  -r <file>   [MANDATORY] Short reverse strand reads in FASTQ format (.fastq or .fastq.gz)
  -l <file>   [MANDATORY] Long reads in FASTQ format (.fastq or .fastq.gz)

  Output:
  -n <str>    [Optiona] Sample name, ID or alias

  Parameters:
  -M <str>    [Optional] Choose mode from list: conservative, normal, bold
  -c <int>    [MANDATORY] Minimum coverage to subset long reads -- for a typical analysis, 
              we recommend a value of '20'; for completion mode, we recommend a value of '5'
  -p <int>    [Optional] Minimum Phred score threshold for short read trimming. Default=20
  
  Others:
  -E <str>    [Optional] Indicate the name of the conda environment where Snakemake is installed;
              if not provided, will try to use environment "base". This option is only valid if 
              running the pipeline outside of container mode.
  -S          [Optional] Singularity mode: use software installed in a singularity container,
              instead of Conda environments
  -v          Print pipeline version.
  -h          Print this Help page and exit.
HELP_USAGE
}

# Parse options
use_conda=true
while getopts ":f:r:l:n:M:c:p:Sh" opt; do
  case $opt in
    f)
      forward=$OPTARG
      ;;
    r)
      reverse=$OPTARG
      ;;
    l)
      long_reads=$OPTARG
      ;;
    n)
      name=$OPTARG
      ;;
    E)
      snakemake_env=$OPTARG
      ;;
    M)
      mode=$OPTARG
      ;;
    c)
      coverage=$OPTARG
      ;;
    p)
      phred_score=$OPTARG
      ;;
    S)
      use_conda=false
      ;;
    v)
      printversion
      exit 0
      ;;
    h)
      usage 
      exit 0
      ;;
    \?)
      echo -e "\n"
      echo "Invalid option: -$OPTARG" >&2
      echo "Check that you have provided all the inputs"
      usage
      exit 1
      ;;
    :)
      echo -e "\n"
      echo "Error: Option -$OPTARG requires an argument." >&2
      usage
      exit 1
      ;;
  esac
done

# Error handling
if [ -z "$forward" ]; then
  echo -e "\n Error: Oops, it seems that you are missing the Illumina forward files.\n"
  usage
  exit 1
fi

if [ -z "$reverse" ]; then
  echo -e "\n Error: Oops, it seems that you are missing the Illumina reverse files.\n"
  usage
  exit 1
fi

if [ -z "$long_reads" ]; then
  echo -e "\n Error: Oops, it seems that you are missing the ONT reads.\n"
  usage
  exit 1
fi

if [ -z "$name" ]; then
  echo -e "\n Error: Oops, it seems that you forgot to set a (sample) name for your analysis.\n"
  usage
  exit 1
fi

if [ -z "$mode" ]; then
  mode="normal"
  echo -e "\nUnicycler will be run using ${mode} mode.\n"
else
  case "$mode" in
    conservative|normal|bold)
      echo -e "\nUnicycler will be run using ${mode} mode.\n"
      ;;
    *)
      usage
      exit 1
      ;;
  esac
fi

if [ -z "$coverage" ]; then
  echo -e "\n Error: Oops, it seems that you are missing the coverage that you want to use to subset the ONT reads.\n"
  exit 1
fi

if [ -z "$phred_score" ]; then
  echo -e "A phred score of 20 will be used to trim the Illumina reads\n"
  phred_score=20
fi

echo "######################################################################"
echo "#  Running Hybrid Assembly pipeline ${version} in assembly completion mode  #"
echo "######################################################################"

# Save config template per sample
mkdir -p templates
cat <<EOT >>templates/"${name}"_path_assembly.yaml
forward: "${forward}"
reverse: "${reverse}"
long: "${long_reads}"
name: "${name}"
unicycler_mode: "${mode}"
coverage: "${coverage}"
phred_score: "${phred_score}"
EOT

# Run snakemake workflow
# 1. Use conda environments for snakemake rules
if [ "$use_conda" == true ]; then
  # Activate user-provided environment with snakemake installed
  if $snakemake_env; then
    source activate $snakemake_env
  else
    source activate base
  fi

	snakemake \
  --configfile templates/"$name"_path_assembly.yaml \
  --snakefile src/assembly.smk \
  --latency-wait 60 \
  --verbose \
  --forceall \
  -p long_read_assembly/"$name"_path_unicycler \
  --keep-going \
  --restart-times 1 \
  --use-conda \
  --cluster-config config/config.json \
  --cluster \
  'sbatch --mem={cluster.mem} -t {cluster.time} -c {cluster.c}' \
  --jobs 10 2>&1

# 2. Don't use conda environments for snakemake rules
elif [ "$use_conda" == false ]; then
  snakemake \
  --configfile templates/"$name"_path_assembly.yaml \
  --snakefile src/assembly.smk \
  --latency-wait 60 \
  --verbose \
  --forceall \
  -p long_read_assembly/"$name"_path_unicycler \
  --keep-going \
  --restart-times 1 \
  --jobs 10 2>&1
fi

exit 0